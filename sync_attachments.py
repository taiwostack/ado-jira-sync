import httpx, json
from requests.auth import HTTPBasicAuth

from atlassian import Jira

def get_workitem_attachments(token, url, workitem_id):
    """
        Returns a list of attachments belonging to a workitem

        :params token: the personal access token for the ADO instance
        :params url: the url of the ADO instance
        :params workitem_id: the id of the workitem
    """
    client = httpx.Client(http2=True)
    client.headers = {"Accept": "application/json"}
    client.auth = HTTPBasicAuth("", token)
    client.base_url = url

    response = client.get(f"/_apis/wit/workitems/{workitem_id}?api-version=5.0",params={"$expand":"Relations"})
    if response.status_code == 200:
        response = response.json()
        relations = response["relations"] if response["relations"] else []
        return  [{**x["attributes"], "url":x["url"]} for x in relations if x["rel"] == "AttachedFile"]
    
    # raise an exception for any response other than a 200
    response.raise_for_status()

def compare(jira_issue_id, ADO_workitem_id, jira_auth_dict, ado_auth_dict, leeway=0):
    """
    Returns True if both jira and ado  issues have the same attachments else False
    Currently works for only server instances of both 

    :params jira_issue_id: The id or key of a particular jira issue
    :params ADO_workitem_id: The id of particular workitem
    :params jira_auth_dict: a dict containing authentication details for jira instance {url:str, username:str, password:str}
    :params ado_auth_dict: a dict containing authentication details for the ADO instance {url:str, token:str}
    
    :params leeway: optional param that defines how close to each other the attachment file sizes can be
                    and still be seen as equivalent.
                    For example: a leeway of 2 where one attachment's size is 41 and the other is 43 means that both will be considered as equivalent
                    a leeway of zero means the file size has to be exactly the same for both
    """

    # ensure that a negative number isnt added as leeway
    leeway = 0 if leeway < 0 else leeway

    # authenticate and fetch the jira's issue attachments metadata
    jira = Jira(
            url=jira_auth_dict["url"],
            username=jira_auth_dict["username"],
            password=jira_auth_dict["password"], 
        )
    jira_attachments = jira.issue_field_value(jira_issue_id, "attachment")
    jira_attachments = [{"filename":j["filename"], "size":j["size"], "created":j["created"]} for j in jira_attachments]

    # fetch workitem attachment data
    workitem_attachments = get_workitem_attachments(ado_auth_dict["token"], ado_auth_dict["url"], ADO_workitem_id)
    workitem_attachments = [{"filename":data["name"], "size":data["resourceSize"], "created":data["resourceCreatedDate"]} for data in workitem_attachments]
    
    # quickly returns false if both issues do not have the same number of attachments
    if len(jira_attachments) != len(workitem_attachments):
        return False

    # sort the attachments so that they line up
    workitem_attachments.sort(key=lambda x: x["filename"])
    jira_attachments.sort(key=lambda x: x["filename"])

    # compare file names
    all_same_file_names = all(
            [
                workitem_attachments[i]["filename"] == jira_attachments[i]["filename"]
                for i in range(len(jira_attachments))
            ]
        )
    
    # compare file sizes
    all_same_file_size = all(
            [
                abs(workitem_attachments[i]["size"] - jira_attachments[i]["size"]) <= leeway 
                for i in range(len(jira_attachments))
            ]
        )
    
    return all_same_file_names and all_same_file_size

def get_missing_attachments(jira_issue_id, ADO_workitem_id, jira_auth_dict, ado_auth_dict, leeway=0):
    """
    Returns a list of attachments that exist in jira but not ado and vice versa
    only works for server instances right now 

    :params jira_issue_id: The id or key of a particular jira issue
    :params ADO_workitem_id: The id of particular workitem
    :params jira_auth_dict: a dict containing authentication details for jira instance {url:str, username:str, password:str}
    :params ado_auth_dict: a dict containing authentication details for the ADO instance {url:str, token:str}
    
    :params leeway: optional param that defines how close to each other the attachment file sizes can be
                    and still be seen as equivalent.
                    For example: a leeway of 2 where one attachment's size is 41 and the other is 43 means that both will be considered as equivalent
                    a leeway of zero means the file size has to be exactly the same for both
    """

    # ensure that a negative number isnt added as leeway
    leeway = 0 if leeway < 0 else leeway

    # authenticate and fetch the jira's issue attachments metadata
    jira = Jira(
            url=jira_auth_dict["url"],
            username=jira_auth_dict["username"],
            password=jira_auth_dict["password"], 
        )
    jira_attachments = jira.issue_field_value(jira_issue_id, "attachment")
    jira_attachments = {j["filename"]:{"size":j["size"], "created":j["created"], "url":j["content"]} for j in jira_attachments}

    # fetch workitem attachment data
    workitem_attachments = get_workitem_attachments(ado_auth_dict["token"], ado_auth_dict["url"], ADO_workitem_id)
    workitem_attachments = { data["name"]:{"size":data["resourceSize"], "created":data["resourceCreatedDate"], "url":data["url"]} for data in workitem_attachments}
    

    # get the filenames that belong in one but not the other using set difference
    filenames_in_jira_and_not_ado = jira_attachments.keys() - workitem_attachments.keys()
    filenames_in_ado_and_not_jira = workitem_attachments.keys() - jira_attachments.keys()

    # get the filenames that exist in both
    found_in_both = set(jira_attachments.keys()).intersection(set(workitem_attachments.keys()))
    
    # for the filenames that exist in both ensure that thier file sizes are similar
    for attach in found_in_both:
        if abs(workitem_attachments[attach]["size"] - jira_attachments[attach]["size"]) > leeway:
            filenames_in_ado_and_not_jira.add(attach)
            filenames_in_jira_and_not_ado.add(attach)

    # return the filenames that were found in one but not the other
    result = [
        {"filename":name, "source":"jira", "target":"ado", "file_created":jira_attachments[name]["created"], "path":jira_attachments[name]["url"]}
            for name in filenames_in_jira_and_not_ado
        ]

    result.extend([
        {"filename":name, "source":"ado", "target":"jira", "file_created":workitem_attachments[name]["created"], "path":workitem_attachments[name]["url"]}
            for name in filenames_in_ado_and_not_jira
        ])

    return result

